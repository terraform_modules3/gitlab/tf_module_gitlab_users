
variable "users" {
  description = <<EOT
  https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/user


  Notes:
  If group_id is specified, group_full_path is ignored
  If project_id is specified, project_full_name is ignored

  Example:
  users = [
    { 0 = {
      name     = "Jonathan Swift"
      email    = "some@email.com"
      username = "user123"
      groups = [
        {
          group_id     = 23
          access_level = "developer"
          expires_at   = "2099-12-31"
        },
      ]
      projects = [
        {
          access_level = "developer"
          project_id = 8
        },
      ]
    } },
  ]
  EOT
  type = list(map(
    object({
      name     = string
      email    = string
      username = string

      generate_initial_password = optional(bool, false),
      can_create_group          = optional(bool, false),
      is_admin                  = optional(bool, false),
      is_external               = optional(bool, false), # External users can only access projects to which they are explicitly granted access
      note                      = optional(string, ""),
      password                  = optional(string, ""),
      projects_limit            = optional(number, 0),
      skip_confirmation         = optional(bool, true),
      state                     = optional(string, "active"), # active, deactivated, blocked


      groups = optional(list(object({
        group_full_path = optional(string)
        group_id        = optional(number)
        access_level    = optional(string, "guest")
        expires_at      = optional(string, null)
      })), []),

      projects = optional(list(object({
        project_full_name = optional(string)
        project_id        = optional(number)
        access_level      = optional(string, "guest")
        expires_at        = optional(string, null)
      })), []),
    })
  ))
  default = []
}
