terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
  required_version = ">= 1.5.7"
}
