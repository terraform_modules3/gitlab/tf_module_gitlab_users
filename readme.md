# Gitlab users Terraform module
Module to create and manage users in Gitlab

Allows to create group and project memberships and generate passwords

## Usage example
```hcl
module "users" {
  source = "git::https://gitlab.com/terraform_modules3/gitlab/tf_module_gitlab_users.git?ref=0.1.0"

  users = [
    { 0 = {
      name                      = "Jonathan Swift"
      email                     = "some@email.com"
      username                  = "user123"
      generate_initial_password = true
    } },
    { 1 = {
      name                      = "another user"
      email                     = "another@email.com"
      username                  = "user-abc"
      generate_initial_password = true
    } },
    { 2 = {
      name     = "some user"
      email    = "user@email.com"
      username = "username1"
      groups = [
        {
          group_id     = 23
          access_level = "developer"
          expires_at   = "2099-12-31"
        },
        {
          group_full_path = "group1"
          access_level    = "guest"
        },
        {
          group_id     = 17
          access_level = "developer"
        },
      ]
      projects = [
        {
          access_level      = "developer"
          expires_at        = "2099-12-31"
          project_full_name = "group22/group11/repo22"
        },
        {
          access_level = "developer"
          project_id   = 8
        },
      ]
    } },
  ]
}

```

## Output example
```hcl
initial_passwords = {
  "0" = {
    "pass" = "nwyuIp6WVQa1oPrz-change-me"
    "user" = "some@email.com"
  }
  "1" = {
    "pass" = "O6O2OJQxy9neKKDx-change-me"
    "user" = "another@email.com"
  }
}
users = {
  "username1" = {
    "email" = "user@email.com"
    "id" = "10"
    "is_admin" = false
    "name" = "some user"
    "note" = ""
    "state" = "active"
  }
  "user123" = {
    "email" = "some@email.com"
    "id" = "7"
    "is_admin" = false
    "name" = "Jonathan Swift"
    "note" = ""
    "state" = "active"
  }
  "user-abc" = {
    "email" = "another@email.com"
    "id" = "8"
    "is_admin" = false
    "name" = "another user"
    "note" = ""
    "state" = "active"
  }
}
```
