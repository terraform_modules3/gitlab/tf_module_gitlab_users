locals {
  project_memberships = toset(flatten([
    for user in var.users : [
      for project in values(user)[0].projects : merge(
        { user_index = keys(user)[0] },
        {
          project_id   = project.project_id == null ? tonumber(data.gitlab_project.project[project.project_full_name].id) : project.project_id
          access_level = project.access_level
          expires_at   = project.expires_at
        }
      )
    ]
  ]))

  projects_full_names = toset(flatten([
    for user in var.users : [
      for project in values(user)[0].projects :
      project.project_full_name
      if project.project_id == null
    ]
  ]))
}

data "gitlab_project" "project" {
  for_each            = local.projects_full_names
  path_with_namespace = each.value
}
resource "gitlab_project_membership" "membership" {
  for_each = { for membership in local.project_memberships : "${membership.user_index}:${membership.project_id}" => membership }

  project      = each.value.project_id
  user_id      = gitlab_user.user[each.value.user_index].id
  access_level = each.value.access_level
  expires_at   = each.value.expires_at

  depends_on = [gitlab_user.user]
}
