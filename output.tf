output "users" {
  value = local.users
}

# tf output -json |jq -rc '.initial_passwords.value[] | "\(.user) \(.pass)" '
output "initial_passwords" {
  value = local.initial_passwords
}
