locals {
  group_memberships = toset(flatten([
    for user in var.users : [
      for group in values(user)[0].groups : merge(
        { user_index = keys(user)[0] },
        {
          group_id     = group.group_id == null ? tonumber(data.gitlab_group.group[group.group_full_path].id) : group.group_id
          access_level = group.access_level
          expires_at   = group.expires_at
        }
      )
    ]
  ]))

  groups_full_paths = toset(flatten([
    for user in var.users : [
      for group in values(user)[0].groups :
      group.group_full_path
      if group.group_id == null
    ]
  ]))
}

data "gitlab_group" "group" {
  for_each  = local.groups_full_paths
  full_path = each.value
}

resource "gitlab_group_membership" "membership" {
  for_each = { for membership in local.group_memberships : "${membership.user_index}:${membership.group_id}" => membership }

  group_id     = each.value.group_id
  user_id      = gitlab_user.user[each.value.user_index].id
  access_level = each.value.access_level
  expires_at   = each.value.expires_at

  depends_on = [gitlab_user.user]
}
