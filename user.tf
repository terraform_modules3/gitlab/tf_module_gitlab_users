locals {
  # for output
  users = {
    for user_info in gitlab_user.user :
    user_info.username => {
      id       = user_info.id
      name     = user_info.name
      email    = user_info.email
      is_admin = user_info.is_admin
      note     = user_info.note
      state    = user_info.state
    }
  }

  initial_passwords = {
    for user in var.users : keys(user)[0] => {
      user : values(user)[0].email
      pass : "${random_string.initial_password[keys(user)[0]].result}-change-me"
    } if values(user)[0].generate_initial_password
  }
}

resource "gitlab_user" "user" {
  for_each = { for user in var.users : keys(user)[0] => values(user)[0] }

  name              = each.value.name
  username          = each.value.username
  email             = each.value.email
  password          = each.value.generate_initial_password ? "${random_string.initial_password[each.key].result}-change-me" : null
  can_create_group  = each.value.can_create_group
  is_admin          = each.value.is_admin
  is_external       = each.value.is_external
  note              = each.value.note
  projects_limit    = each.value.projects_limit
  reset_password    = each.value.generate_initial_password ? false : true # Send user password reset link
  skip_confirmation = each.value.skip_confirmation
  state             = each.value.state

  lifecycle {
    prevent_destroy = true
    ignore_changes  = [password]
  }
}

# tflint-ignore: terraform_required_providers
resource "random_string" "initial_password" {
  for_each = { for user in var.users : keys(user)[0] => values(user)[0] }

  length  = 16
  special = false
}
